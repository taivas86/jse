//F6. Write a JavaScript function that accepts a string as a parameter and find the longest word within the string. Go to the editor
//rewrite with cycle
    function longVehicle(str){
        arr = str.split(' ');
        tmp = [];
        for(i = 0; i < arr.length; i++){
            for( j = i + 1; j < arr.length; j++){
                if(arr[i] > arr[j]){
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        return arr.join(' ');
    }

    console.log(longVehicle('Web Development Tutorial'));
