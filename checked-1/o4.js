//0-4. Write a JavaScript program to display the reading status (i.e. display book name, author name and reading status) of the following books.

    var library = [
        {
            author: 'Bill Gates',
            title: 'The Road Ahead',
            readingStatus: true
        },
        {
            author: 'Steve Jobs',
            title: 'Walter Isaacson',
            readingStatus: true
        },
        {
            author: 'Suzanne Collins',
            title: 'Mockingjay: The Final Book of The Hunger Games',
            readingStatus: false
        }
    ];

    function getBookStatus(obj) {

        for (i = 0; i < obj.length; i++) {
            console.log(
                "Author: " + obj[i].author + ", " +
                "Title: " + obj[i].title + ", " +
                "ReadingStatu: " + obj[i].readingStatus
            );
        }

    }

    console.log(getBookStatus(library));

