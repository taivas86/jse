//F7. Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string. Go to the editor
//Note : As the letter 'y' can be regarded as both a vowel and a consonant, we do not count 'y' as vowel here.

function vowels(string){
    var m = string.match(/[aeiou]/gi);
    return m == null ? 0 : m.length;
}

console.log(vowels('The quick brown fox'));
