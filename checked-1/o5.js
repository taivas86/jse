//0bj-5. Write a JavaScript program to get the volume of a Cylinder with four decimal places using object classes. Volume of a cylinder : V = πr(2)h where r is the radius and h is the height of the cylinder.
//fix 4 decimal

    //js constructor (как всегда девиз js: стоя делали, половина вытекла)
    function Cylinder(height, radius) {
        this.height = height;
        this.radius = radius;
    }
    

    //метод "класса" он же прототип, содержащий пустой объект function.prototype
    Cylinder.prototype.getVolume = function () {
        return (Math.PI * Math.pow(this.radius, 2) * this.height).toFixed(4);
    }

    var cyl = new Cylinder(4, 5);
    console.log(cyl.getVolume());
