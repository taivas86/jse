//F9. Write a JavaScript function which accepts an argument and returns the type. Go to the editor
//Note : There are six possible values that typeof returns: object, boolean, function, number, string, and undefined.

function getType(dataType){
    return typeof dataType;
}

console.log(getType(false));
