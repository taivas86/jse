//F8. Write a JavaScript function that accepts a number as a parameter and check the number is prime or not. Go to the editor
//Note : A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself.

//This is 6K+-1 algorithm from C++;

    function isPrime(n) {
        if (n <= 1) {
            return false;
        }

        if (n <= 3) {
            return true;
        }

        if (n % 2 == 0 || n % 3 == 0) {
            return false;
        }


        i = 5;
        while (i * i <= n) {
            if (n % i === 0 || n % (i + 2) == 0) {
                return false;
            }
            i += 6;


        }

        return true;
    }

    console.log(isPrime(10))

//rewrite c++ code
//https://www.geeksforgeeks.org/primality-test-set-1-introduction-and-school-method/
function  isPrimeC(n) {
	if(n<=1) false;
	if(n<=3) true;

	//check to skip middle 5 digits
	if(n%2==0 || n%3===0) return false;
	
	for (i=5; i*i<=5; i=i+6) {
		if(n%i==0 || n%(i+2)==0
		return false;	
	}

} 
console.log(isPrimeC(12));
