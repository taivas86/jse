function Person(currentAge, maxAge, money) {
    this.currentAge = currentAge;
    this.maxAge = maxAge;
    this.days = money;
}

let person = new Person(22,88,220);

Person.prototype.getTotalAmount = function () {
    return (this.maxAge - this.currentAge) * 365 * this.days;
}

console.log("You will need " + person.getTotalAmount() + " to last you until the ripe old age of "+ person.maxAge);
