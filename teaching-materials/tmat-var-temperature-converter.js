function convertFarToCel(fars) {
    return (fars - 32) * 0.5556;
}

function convertCelToFar(cels) {
    return (cels * 1.8) + 32;
}

function convert(fn, temp) {
    console.log(fn(temp));
}

convert(convertCelToFar,20);