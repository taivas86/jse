class Circle {

    constructor(radius) {
        this.radius = radius;
    }
}

Circle.prototype.getCurcumference= function() {
    console.log("The circumference is " + (2 * Math.PI * this.radius));
}

Circle.prototype.getArea =  function() {
    console.log("The area is " + (Math.PI * Math.pow(this.radius, 2)));
}


object = new Circle(10);
object.getCurcumference();
object.getArea();