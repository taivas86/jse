class Person {

    set setInitYear(yearInit) {
        this.yearInit = yearInit;
    }

    set setCurrentYear(yearCurrent){
        this.yearCurrent = yearCurrent;
    }

    get getInitYear(){
        return this.yearInit;
    }

    get getCurrentYear() {
        return this.yearCurrent;
    }
}

const man = new Person();
man.setInitYear = 1986;
man.setCurrentYear = 2020;

console.log(man.yearCurrent - man.yearInit);
