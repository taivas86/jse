class Doggy {
    constructor(age) {
        this.age = age;
    }

    calculateAgeDog() {
        return "Your doggie is:" + (this.age * 7) + " years old in dog years!"
    }
}

class Man extends Doggy {
    calculateAgeMan() {
        return "Your age is: " + Math.floor(this.age / 7) + " years old in dog years!"
    }
}

const dog = new Doggy(15);
console.log(dog.calculateAgeDog());

const man = new Man(88);
console.log(man.calculateAgeMan());

