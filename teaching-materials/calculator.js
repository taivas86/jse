function squareNumber(number) {
    return Math.pow(number, 2);
}

function halfNumber(number) {
    return "Half of " + number + " is " + number / 2;
}

function percentOf(num1, num2) {
    let result = num1 * (num2 / 100);
    return result + " is " + num2+"%" + " of " + num1;
}

function areaOfCircle(radius) {
    return (Math.PI * Math.pow(radius, 2)).toFixed(2);
}




