class Person {
    constructor(name, childNum, partnerName, location, job) {
        this.name = name;
        this.childNum = childNum;
        this.partnerName = partnerName;
        this.location = location;
        this.job = job;
    }

    get personName() {
        return this.name;
    }

    get personCity() {
        return this.location;
    }

    get marriedName() {
        return this.partnerName;
    }

    get childQnt() {
        return this.childNum;
    }

    get jobTitle() {
        return this.job;
    }
}

var dracula = new Person("frei", '2', 'fedora', 'led');

console.log("You will be a: " + dracula.personName
                              + " in " + dracula.personCity
                              + " and married to " + dracula.marriedName
                              + " with " + dracula.childQnt + " kids");



