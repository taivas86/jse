//C4. Write a function that when given a URL as a string, parses out just the domain name and returns it as a string.  domainName("http://github.com/carbonfive/raygun") == "github"
//расшифровка регулярки - сопоставление с подстрокой "/" в обработке строки с url

function domainName(url){
       var domain= url.replace('http://', '').replace('https://', '').replace('www.', '').split(/[/?#]/)[0];
       cleared = domain.split(".");
       return cleared[0];
}
