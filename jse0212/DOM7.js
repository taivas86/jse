//DOM-7. Write a JavaScript function that creates a table, accept row, column numbers from the user, and input row-column number as content (e.g. Row-0 Column-0) of a cell.

    function inputData(){
        let rw = prompt('Insert row','0');
        let cl = prompt('Insert cell','0');

        return {rw, cl};
    }

    function createTable() {

        let data = inputData();

        for (let i = 0; i <= data.rw; i++) {
            let rows = document.getElementById('myTable').insertRow(i);
            for (let j=0; j<=data.cl; j++) {
                let cols = rows.insertCell(j);
                cols.innerText = 'row'+i+ " " + 'cell'+j;
            }
            
        }
 }