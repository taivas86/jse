//ERX9
//opening/closing, withdrawals, and deposits of card.

    function Depo(fname, lname, sum) {

        this.fname = fname;
        this.lname = lname;

        if ((this.fname == undefined) || (this.lname == undefined)) {
            throw new Error('noname user');
        }
        this.fname = fname;

        this.sum = sum;
        if (this.sum == undefined) {
            this.sum = 0;
        }
    }

    Depo.prototype.withdrawal = function (sumWithdrawals) {
        if (this.sum - sumWithdrawals < 0)
            throw new Error('debit card. tech overdraft denied');

        return this.sum - sumWithdrawals;
    }

    Depo.prototype.deposit = function (sumDeposit) {
        if (sumDeposit == undefined || sumDeposit <= 0)
            throw new Error('incorrect deposit sum');

        return this.sum + sumDeposit;
    }

    var card7296 = new Depo('lorem', 'ipsum', 12000);
    //card7296 = card7296.withdrawal(3000);
    //console.log(card7296);

    function closeDepo(account) {
        return account = null;
    }

    console.log(closeDepo(card7296));
