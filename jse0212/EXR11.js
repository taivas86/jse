//EXR 11

function godovasik13(question) {

    if (question == 'How are you?') {
        return "Sure";
    }

    if (question == undefined || question.replace(/\s+/g, '') === '') {
        return "Fine. Be that way!";
    }

    if (question !== 'How are you' && question.includes('?')) {
        return "Calm down, I know what I'm doing!";
    }

    if (question === question.toUpperCase()) {
        return "Whoa, chill out!";
    }

    if (question) {
        return 'Whatever.';
    }
}
