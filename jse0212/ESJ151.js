//ESJ151. В промежутке с 1928 по 2013 год турецкие законы запрещали использование букв Q, W и X в официальных документах. Запрограммировать поле для ввода текста так, чтобы эти буквы нельзя было туда вписать

    var field = document.querySelector("input");
    tmp = ['q', 'w', 'x'];

    field.addEventListener('keypress', function (event) {

           tmp.forEach(elt => {
                if (event.key === elt) {
                    event.preventDefault();
                }
            });
    });