//exorcism #3

    function resistorColor(color) {

        let colors = ['black', 'brown', 'red', 'orange', 'yellow', 'green', 'blue', 'violet', 'gray', 'white'];
        return colors.indexOf(color);
    }

    console.log(resistorColor('white'))
