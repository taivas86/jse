//EXR5

    function gigaSecond(date){

        let dateNew = Math.floor(Date.parse(date) / 1000);
        return new Date((dateNew+Math.pow(10,9)) * 1000);
    }

    console.log(gigaSecond('2020.02.05'));

