//DOM-9. Write a JavaScript program to count and display the items of a dropdown list, in an alert window

    function getOptions() {
        let elms = document.getElementById('mySelect');
        let tmp = [];

        for (i = 0; i < elms.length; i++) {
            tmp.push (elms[i].value);
        }

        alert(
            "Qnt:" + tmp.length + "\n"+
            "List of values: " + tmp.join(", ")
        );

    }
