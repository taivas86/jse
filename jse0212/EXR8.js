//EXR8

const ALPHABETS = 'abcdefghijklmnopqrstuvwxyz';

function panagram(string) {
  
    chars = ALPHABETS.split("");
    string = chars.toLowerCase();

    return chars.every(x=>string.includes(x));
}

console.log(panagram('The quick brown fox jumps over the lazy dog'));
