//EXR6
    function getRNA(pattern){

        rna = '';

        broken = /[^G,C,T,A]/i;

        if(pattern.match(broken)) {
            throw new Error ('Invalid pattern');
        }

        for (let i=0; i<pattern.length; i++){
            if(pattern[i]==='G') rna+='C';
            if(pattern[i]==='C') rna+='G';
            if(pattern[i]==='T') rna+='A';
            if(pattern[i]==='A') rna+='U';
        }

        return pattern + " to RNA = "+rna;

    }

    console.log(getRNA('ATTG'))
