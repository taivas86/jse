/**
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <div class="header">
    </div>
    <section id="container">
        <ul>
            <li class="first">one</li>
            <li class="second">two</li>
            <li class="third">three</li>
        </ul>
        <ol>
            <li class="first">one</li>
            <li class="second">two</li>
            <li class="third">three</li>
        </ol>
    </section>
    <div class="footer">
    </div>
</body>

</html>
*/

    var sectionById = document.getElementById('container');
    //console.log(sectionById);

    var sectionBySelector = document.body.querySelector('section');
    //console.log(sectionBySelector);

    var secondItems = document.body.getElementsByClassName('second');
    //console.log(secondItems);


    var tmp = document.body.querySelector('ol').getElementsByClassName('third');
    //console.log(tmp);


    var parent = document.getElementById('container');
    var text = document.createElement('p').innerText = 'hello';
    //parent.append(text);

    footer = document.body.getElementsByClassName('footer')[0];
    divF = document.createElement('div');
    divF.className = 'main';
    footer.append(divF);

    rmElt = document.body.getElementsByClassName('main')[0];
    rmElt.remove();

    var ul = document.body.querySelector('ul');
    liElt = document.createElement('li');
    liElt.innerText = 'four';
    ul.append(liElt);

    ulli = document.getElementsByTagName('ol');

    for (i = 0; i <= ulli.length; i++) {
        ulli[i].style.background = "green";
    }

    var footerRM = document.getElementsByClassName('footer')[0];
    footerRM.remove();
