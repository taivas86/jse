//R3. Напишите функцию fib(n) которая возвращает n-е число Фибоначчи.

function fbn(number) {
    if (number === 0) return 0;
    if (number === 1) return 1;

    return fbn(number - 1) + fbn(number - 2);
}

function fibLoop(n) {
    let a = 1;
    let b = 1;

    for (let i = 3; i <= n; i++) {
        let c = a + b;
        a = b;
        b = c;
    }

    return b;

}

console.log(fibLoop(77))
