//DOM-10. Write a JavaScript program to calculate the volume of a sphere (2 input field)

function calc() {
    var radius = document.getElementById('radius').value;
    document.getElementById('volume').value = 1.33*Math.PI*radius;
};
