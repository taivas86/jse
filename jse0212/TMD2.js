//the book list
/**
<!DOCTYPE html>
<html>

<head>
	<title>Morning JS DOM</title>
</head>

<body>
	<h1>MyBookList</h1>

</body>

</html>
*/

	var books=[{title:"vvo",author:"lorem",alreadyRead:1, url:"https://outlooker.ru/wp-content/uploads/2020/12/vendetta-prev.jpg"},{title:"led",author:"ipsum",alreadyRead:0,url:"https://outlooker.ru/wp-content/uploads/2020/12/vendetta-prev.jpg"},{title:"khv",author:"deloren",alreadyRead:1,url:"https://outlooker.ru/wp-content/uploads/2020/12/vendetta-prev.jpg"},{title:"hel",author:"bells",alreadyRead:0,url:"https://outlooker.ru/wp-content/uploads/2020/12/vendetta-prev.jpg"},{title:"pkc",author:"volcano",alreadyRead:1,url:"https://outlooker.ru/wp-content/uploads/2020/12/vendetta-prev.jpg"}];

	newDiv = document.createElement("div");
	newDiv.className="books__list";
	document.body.appendChild(newDiv);
	books.forEach(elem => {
		para = document.createElement("p");
		para.innerHTML = elem.author + "-" + elem.title;
		newDiv.appendChild(para);
	});

	newDiv = document.createElement("div");
	newDiv.className = "books__ulist";
	document.body.appendChild(newDiv);

	uList = document.createElement("ul");
	uList.className = "book__list";
	newDiv.appendChild(uList);

	books.forEach(elem => {
		para = document.createElement("li");
		para.innerHTML = elem.author + "-" + elem.title;

		img = new Image();
		img.src = elem.url;

		para.appendChild(img);
		uList.appendChild(para);

		if (elem.alreadyRead == 0) {
			para.style.color="red";
			para.style.fontWeight = "bold";
		}
	});


