//R1. Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n. Циклом и рекурсией.

    function sumTo(n) {

        if (n == 1) {
            return 1;
        } else {
            return n + sumTo(n - 1);
        }
    }

    console.log(sumTo(3));

    function sumTo(n) {
        let sum = 0;
        for (let i = 1; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    alert(sumTo(100));
